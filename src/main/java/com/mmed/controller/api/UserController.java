package com.mmed.controller.api;

import com.mmed.domain.User;
import com.mmed.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author mmed 4/19/18
 */
@RestController
public class UserController {

    @Resource(name = "user_service")
    private UserService userService;

    @RequestMapping(path = "/api/v1/user", method = RequestMethod.GET)
    public ResponseEntity<List<User>> get() {
        List<User> userList = userService.getAll();
        return ResponseEntity.ok(userList);
    }


    @RequestMapping(path = "/api/v1/user", method = RequestMethod.POST)
    public ResponseEntity<List<User>> get(@RequestParam Long id, @RequestParam Boolean greater) {
        List<User> userList = userService.getUserAndFilterById(id, greater);

        return ResponseEntity.ok(userList);
    }

}
