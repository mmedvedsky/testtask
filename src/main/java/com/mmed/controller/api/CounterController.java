package com.mmed.controller.api;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicLong;

/**
 * @author mmed 4/19/18
 */
@RestController
public class CounterController {

    private AtomicLong counter = new AtomicLong(0);

    @RequestMapping(path = "/api/v1/counter", method = RequestMethod.GET)
    public ResponseEntity<Long> getCounter() {
        Long result = counter.getAndIncrement();

        return ResponseEntity.ok(result);
    }
}
