package com.mmed.domain;

/**
 * @author mmed 4/19/18
 */
public class UserImpl implements User {

    private Long id;
    private Long userId;
    private String title;
    private String body;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public Long getUserId() {
        return userId;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getBody() {
        return body;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public void setBody(String body) {
        this.body = body;
    }

}
