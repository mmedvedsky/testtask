package com.mmed.domain;

/**
 * @author mmed 4/19/18
 */
public interface User {

    Long getId();
    Long getUserId();

    String getTitle();
    String getBody();

    void setId(Long id);
    void setUserId(Long userId);
    void setTitle(String title);
    void setBody(String body);
}
