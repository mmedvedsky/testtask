package com.mmed.service;

import com.mmed.domain.User;
import com.mmed.domain.UserImpl;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author mmed 4/19/18
 */

public class UserServiceImpl implements UserService {

    private final String resourceUrl;
    private final RestTemplate restTemplate = new RestTemplate();


    public UserServiceImpl(String resourceUrl) {
        this.resourceUrl = resourceUrl;
    }

    @Override
    public List<User> getAll() {
        ResponseEntity<UserImpl[]> response = restTemplate.getForEntity(resourceUrl, UserImpl[].class);

        if(!response.getStatusCode().is2xxSuccessful()) {
            return Collections.emptyList();
        }
        UserImpl[] users = response.getBody();

        return users == null ? Collections.emptyList() : Arrays.asList(users);
    }

    @Override
    public List<User> getUserAndFilterById(Long id, Boolean greater) {
        List<User> result = new ArrayList<>();

        if (id == null || id < 0) {
            return result;
        }

        for (User u: getAll()) {
            final Long userId = u.getUserId();

            if (greater != null && greater) {
                if (userId != null && userId >= id) {
                    result.add(u);
                }
            } else {
                if (userId != null && userId <= id) {
                    result.add(u);
                }
            }

        }
        return result;
    }
}
