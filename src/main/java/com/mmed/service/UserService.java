package com.mmed.service;

import com.mmed.domain.User;

import java.util.List;

/**
 * @author mmed 4/19/18
 */
public interface UserService {

    List<User> getAll();

    List<User> getUserAndFilterById(Long id, Boolean greater);
}
